------------------------------------------
--- Set Variables ---
------------------------------------------
MQTT_BROKER = mqttBroker
MQTT_BROKER_PORT = mqttBrokerPort
MQTT_BROKER_SECURE = 0

MQTT_PUBLISH_TOPIC = mqttPublishTopic
MQTT_PUBLISH_TOPIC_QoS = 0
MQTT_PUBLISH_TOPIC_RETAIN = 0
-- MQTT_PUBLISH_MESSAGE = "Hello MQTT"

MQTT_CLIENT_ID = mqttClientId
MQTT_CLIENT_USER = mqttClientUser
MQTT_CLIENT_PASSWORD = mqttClientPassword
MQTT_CLIENT_KEEPALIVE_TIME = 120
------------------------------------------

dofile("temp.lua")

--- Initiate MQTT Client ---
MQTT_Client = mqtt.Client(MQTT_CLIENT_ID, MQTT_CLIENT_KEEPALIVE_TIME, MQTT_CLIENT_USER, MQTT_CLIENT_PASSWORD)

--- On Connect Event ---
MQTT_Client:on("connect", function(con) 
  print ("connected") 
end)

--- Offline Event ---
MQTT_Client:on("offline", function(con) 
  print ("offline") 
end)

function sendTemp ()
  status, temp, humi = getTemp()
  if status == dht.OK then
    msg = '{"temp":'..temp..',"humidity":'..humi..'}'
    MQTT_Client:publish(MQTT_PUBLISH_TOPIC, msg, MQTT_PUBLISH_TOPIC_QoS, MQTT_PUBLISH_TOPIC_RETAIN, function(conn) 
      print("msg Published on MQTT Topic...") 
    end)
  else
    print("Received error, not publishing.")
  end
end

--- Connect to MQTT Broker ----
MQTT_Client:connect(MQTT_BROKER, MQTT_BROKER_PORT, MQTT_BROKER_SECURE, function(conn) 
  print("connected")
  sendTemp()
  if not tmr.create():alarm(60000, tmr.ALARM_AUTO, sendTemp)
  then
    print("Unable to create alarm.")
  end
end)


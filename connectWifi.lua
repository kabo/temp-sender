dofile("variables.lua") -- has IPs, IDs, passwords, etc.
function startup ()
  dofile("sendMessage.lua")
end
-- Define WiFi station event callbacks 
wifi_connect_event = function(T) 
  print("Connection to AP("..T.SSID..") established!")
  print("Waiting for IP address...")
  if disconnect_ct ~= nil then disconnect_ct = nil end  
end

wifi_got_ip_event = function(T) 
  -- Note: Having an IP address does not mean there is internet access!
  -- Internet connectivity can be determined with net.dns.resolve().    
  print("Wifi connection is ready! IP address is: "..T.IP)
  print("Startup will resume momentarily, you have 3 seconds to abort.")
  print("Waiting...") 
  tmr.create():alarm(3000, tmr.ALARM_SINGLE, startup)
end
-- Register WiFi Station event callbacks
wifi.eventmon.register(wifi.eventmon.STA_CONNECTED, wifi_connect_event)
wifi.eventmon.register(wifi.eventmon.STA_GOT_IP, wifi_got_ip_event)

wifi.setmode(wifi.STATION) 
wifi.setphymode(wifi.PHYMODE_N)
wifi.sta.clearconfig()
--connect to Access Point (DO NOT save config to flash)
station_cfg={}
station_cfg.ssid=wifiSsid
station_cfg.pwd=wifiPwd
wifiConfigResult = wifi.sta.config(station_cfg)
print(wifiConfigResult)
if not wifiConfigResult then
  print("Couldn't configure wifi.")
end

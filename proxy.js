#!/usr/bin/env node
/* eslint no-console: "off" */
const AWS = require('aws-sdk')
AWS.config.loadFromPath('./awsconf.json')
const proxyConf = require('proxyConf.json')
const iotdata = new AWS.IotData({endpoint: proxyConf.iotEndpoint})
const mqtt = require('mqtt')
const client = mqtt.connect(proxyConf.mqttBroker, {username: proxyConf.mqttUsername, password: proxyConf.mqttPassword})
const moment = require('moment')

client.on('connect', function () {
  client.subscribe(proxyConf.mqttTopic)
})

client.on('message', function (topic, message) {
  // message is Buffer
  //console.log(message.toString())
  const payload = JSON.parse(message)
  payload.source = topic
  payload.ts = moment().unix()
  console.log(payload)
  const params = {
    topic: proxyConf.iotTopic,
    payload: JSON.stringify(payload)
  }
  iotdata.publish(params).promise()
  .then(data => {
    //console.log(data)           // successful response
  })
  .catch(err => {
    console.log(err, err.stack) // an error occurred
  })
})


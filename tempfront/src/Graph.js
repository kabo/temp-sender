import React, { PureComponent } from 'react'
import { withRouter } from 'react-router-dom'
import {connect} from 'react-redux'
import RC2 from 'react-chartjs2'
//import 'chartjs-plugin-annotation'

class Graph extends PureComponent {
  render() {
    const tempData = this.props.tempData // from actual props, not state
    const chartData = {
      datasets: [{
        type: 'scatter',
        label: 'Temp (C)',
        pointBackgroundColor: 'rgba(234, 24, 9, .8)',
        pointBorderWidth: 0,
        pointRadius: 1,
        pointStyle: 'circle',
        showLine: false,
        data: tempData.map(temp => {
          return {
            x: temp.ts,
            y: temp.temp
          }
        })
      }]
    }
    const chartOptions = {
      maintainAspectRatio: false,
      animation: {
        duration: 0
      },
      legend: {
        display: false
      },
      scales: {
        xAxes: [{
          type: 'time',
          time: {
            //unit: 'day',
            displayFormats: {
              minute: 'dd HH:mm',
              hour: 'dd HH:mm',
              day: 'ddd',
              week: 'ddd'
            }
          }
        }]
      }
    }
    return (
      <div className="Graph">
        <RC2 height="200" data={chartData} options={chartOptions} type="line" />
      </div>
    )
  }
}
const mapStateToProps = (state, ownProps) => {
  return {
    dateRange: state.get('dateRange')
  }
}

export default withRouter(connect(mapStateToProps)(Graph))


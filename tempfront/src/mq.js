import {pushTempData} from './actions'
import AWSMqtt from 'aws-mqtt'
//const AWSIoTData = require('aws-iot-device-sdk')

export default class MQ {
  static connect() {
    if (this.mqttClient) {
      return Promise.resolve()
    }
    const AWS = require('aws-sdk')
    let subscribed = false
    return new Promise((resolve, reject) => {
      //this.mqttClient.on('connect', mqttClientConnectHandler);
      //this.mqttClient.on('message', mqttClientMessageHandler)
      //const cognitoIdentity = new AWS.CognitoIdentity()
      AWS.config.credentials.getPromise()
      .then(() => {
        this.mqttClient = AWSMqtt.connect({
          WebSocket: window.WebSocket, 
          region: AWS.config.region,
          credentials: AWS.config.credentials,
          endpoint: process.env.REACT_APP_IOT_ENDPOINT,
          clientId: 'mqtt-client-' + (Math.floor((Math.random() * 100000) + 1)), // clientId to register with MQTT broker. Need to be unique per client 
        })
        const mqttClientConnectHandler = () => {
          //console.log('connect');
          if (!subscribed) {
            this.mqttClient.subscribe(process.env.REACT_APP_IOT_TOPIC, (err, granted) => {
              if (err) return reject(err)
              resolve()
            })
            subscribed = true
            return
          }
          resolve()
        }
        const mqttClientMessageHandler = (topic, payload) => {
          //console.log('received message on topic', topic)
          payload = JSON.parse(payload.toString())
          //console.log('payload', payload)
          window.store.dispatch(pushTempData(payload))
        }
        this.mqttClient.on('connect', mqttClientConnectHandler);
        this.mqttClient.on('message', mqttClientMessageHandler)
      })
      .catch(console.error)
    })
  }
}

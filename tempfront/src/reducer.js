import {Map, fromJS} from 'immutable'

function setState(state, newState) {
  return state.merge(newState)
}
function pushTempData(state, tempData) {
  const curTempData = state.get('tempData')
  if (curTempData.find(d => d.get('ts') === tempData)) return state
  return state.merge({
    tempData: curTempData.push(fromJS(tempData))
  })
}

export default function(state = Map(), action) {
  switch (action.type) { // eslint-disable-line default-case
  case 'SET_STATE':
    return setState(state, action.state)
  case 'SET_LOGIN':
    return setState(state, {login: action.login})
  case 'SET_DATE_RANGE':
    return setState(state, {dateRange: action.dateRange})
  case 'SET_LOADING_TEMP_DATA':
    return setState(state, {loadingTempData: action.loadingTempData})
  case 'SET_TEMP_DATA':
    return setState(state, {tempData: action.tempData})
  case 'PUSH_TEMP_DATA':
    return pushTempData(state, action.tempData)
  }
  return state
}


import React from 'react'
import registerServiceWorker from './registerServiceWorker'
import ReactDOM from 'react-dom';
import {
  HashRouter as Router,
  Route
} from 'react-router-dom'
import {createStore, applyMiddleware, compose} from 'redux'
import {Provider} from 'react-redux'
import ReduxAsyncQueue from 'redux-async-queue'
import AWS, {CognitoIdentityCredentials} from "aws-sdk"
import moment from 'moment'
import { LocaleProvider } from 'antd';
import enUS from 'antd/lib/locale-provider/en_US';
import App from './App'
import reducer from './reducer'
import {setDateRange, loadTempData, init} from './actions'
import MQ from './mq'
import appConfig from './awsconf'
import './index.css'

moment.locale('en-nz')
AWS.config.region = appConfig.region
AWS.config.credentials = new CognitoIdentityCredentials({
  IdentityPoolId: appConfig.IdentityPoolId
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
window.store = createStore(reducer, composeEnhancers(applyMiddleware(ReduxAsyncQueue)))
window.store.dispatch(init())
const dateFrom = moment().subtract(1, 'day').unix()
const dateTo = moment().add(1, 'year').unix()
window.store.dispatch(setDateRange([dateFrom, dateTo]))
window.store.dispatch(loadTempData(dateFrom, dateTo))
MQ.connect()

ReactDOM.render(
  <Provider store={window.store}>
    <LocaleProvider locale={enUS}>
      <Router>
        <Route component={App} />
      </Router>
    </LocaleProvider>
  </Provider>,
  document.getElementById('root')
)
registerServiceWorker()

import React, { Component } from 'react';
import { withRouter } from 'react-router-dom'
import {connect} from 'react-redux'
//import { Switch, Route, Link } from 'react-router-dom'
//import User from './User'
import moment from 'moment'
import Graph from './Graph'
import './App.css';

class App extends Component {
  render() {
    let tempData = this.props.tempData
    const dateRange = this.props.dateRange
    if (dateRange) {
      const from = dateRange[0]
      const to = dateRange[1]
      tempData = tempData.filter(temp => temp.ts >= from && temp.ts <= to)
    }
    tempData.map(temp => {
      temp.ts = moment.unix(temp.ts)
      return temp
    })
    return (
      <div className="App">
        <div className="App-header">
          <h2>Temp!</h2>
        </div>
        <Graph tempData={tempData}/>
      </div>
    );
  }
}
const mapStateToProps = (state, ownProps) => {
  return {
    dateRange: state.get('dateRange').toJS(),
    tempData: state.get('tempData').toJS()
  }
}
export default withRouter(connect(mapStateToProps)(App))


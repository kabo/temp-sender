/*eslint no-console: ["warn", { allow: ["warn", "error"] }] */

import moment from 'moment'

export function init(locationId) {
  return {
    type: 'SET_STATE',
    state: {
      tempData: [],
      dateRange: null,
      loadingTempData: false,
      login: false
    }
  }
}
export function setLoadingTempData(status) {
  return {
    type: 'SET_LOADING_TEMP_DATA',
    loadingTempData: status
  }
}
export function pushTempData(tempData) {
  return {
    type: 'PUSH_TEMP_DATA',
    tempData
  }
}
export function setTempData(tempData) {
  return {
    type: 'SET_TEMP_DATA',
    tempData
  }
}
export function setDateRange(dateRange) {
  return {
    type: 'SET_DATE_RANGE',
    dateRange
  }
}
export function setLogin(login) {
  return {
    type: 'SET_LOGIN',
    login
  }
}

export function loadTempData(from, to) {
  const AWS = require('aws-sdk')
  const documentClient = new AWS.DynamoDB.DocumentClient()
  from = from || moment().subtract(1, 'day').unix()
  to = to || moment().add(1, 'year').unix()
  return {
    queue: 'loadTempQueue',
    callback: (next, dispatch, getState) => {
      dispatch(setLoadingTempData(true))
      const params = {
        TableName: process.env.REACT_APP_DYNAMODB_TABLE,
        KeyConditionExpression: '#source = :source and #ts between :from and :to',
        ExpressionAttributeNames: {
          '#source': 'source',
          '#ts': 'ts'
        },
        ExpressionAttributeValues: {
          ':source': process.env.REACT_APP_DYNAMODB_SOURCE,
          ':from': from,
          ':to': to
        }
      }
      documentClient.query(params).promise()
      .then(r => {
        dispatch(setLoadingTempData(false))
        if (!r.Items) return next()
        dispatch(setTempData(r.Items))
        //r.Items.forEach(item => {
        //  dispatch(pushTempData(item))
        //})
        next()
      })
      .catch(e => {
        console.error(e)
        next()
      })
    }
  }
}

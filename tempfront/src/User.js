import React, { PureComponent } from 'react'
import { Form, Icon, Input, Button } from 'antd'
import {Config, CognitoIdentityCredentials} from "aws-sdk"
//import { AuthenticationDetails, CognitoUserPool, CognitoUser } from 'amazon-cognito-identity-js';
//import {setLogin} from './actions'
import appConfig from './awsconf'

const FormItem = Form.Item

function hasErrors(fieldsError) {
  return Object.keys(fieldsError).some(field => fieldsError[field]);
}

Config.region = appConfig.region
Config.credentials = new CognitoIdentityCredentials({
  IdentityPoolId: appConfig.IdentityPoolId
});
/*
const poolData = {
  UserPoolId : appConfig.UserPoolId,
  ClientId : appConfig.ClientId
}
const userPool = new CognitoUserPool(poolData)
const updateAwsCredentials = (token) => {
  const Logins = {}
  Logins[`cognito-idp.${appConfig.region}.amazonaws.com/${appConfig.UserPoolId}`]
  AWS.config.credentials = new AWS.CognitoIdentityCredentials({
    IdentityPoolId : appConfig.IdentityPoolId, // your identity pool id here
    Logins
  })
}

const getUser = () => {
  const cognitoUser = userPool.getCurrentUser();
  if (cognitoUser != null) {
    cognitoUser.getSession((err, result) => {
      if (result) {
        console.log('You are now logged in.');
        updateAwsCredentials(result.getIdToken().getJwtToken())
        //call refresh method in order to authenticate user and get new temp credentials
        AWS.config.credentials.refresh((error) => {
          if (error) {
            console.error(error)
          } else {
            console.log('Successfully logged!')
          }
        })
        console.log(result.getIdToken())
        setLogin(true)
      }
    })
  }
}

const login = ({Username, Password}) => {
  const authenticationData = { Username, Password }
  const authenticationDetails = new AuthenticationDetails(authenticationData)
  const userData = {
    Username,
    Pool : userPool
  }
  const cognitoUser = new CognitoUser(userData)
  cognitoUser.authenticateUser(authenticationDetails, {
    onSuccess: result => {
      console.log('access token + ' + result.getAccessToken().getJwtToken());
      updateAwsCredentials(result.getIdToken().getJwtToken())

      // Instantiate aws sdk service objects now that the credentials have been updated.
      // example: var s3 = new AWS.S3();

    },
    onFailure: err => {
      console.error(err)
    },
  }) 
}
*/

class UserClass extends PureComponent {
  componentWillMount() {
    //getUser()
    //this.handleSubmit = this.handleSubmit.bind(this)
  }
  componentDidMount() {
    // To disabled submit button at the beginning.
    this.props.form.validateFields();
  }
//  handleSubmit(e) {
//    e.preventDefault();
//    this.props.form.validateFields((err, values) => {
//      if (!err) {
//        console.log('Received values of form: ', values);
//        login(values)
//      }
//    })
//  }
  render() {
    const { getFieldDecorator, getFieldsError, getFieldError, isFieldTouched } = this.props.form;

    // Only show error after a field is touched.
    const usernameError = isFieldTouched('Username') && getFieldError('Username');
    const passwordError = isFieldTouched('Password') && getFieldError('Password');
    return (
      <div className="User">
        <Form layout="inline" onSubmit={this.handleSubmit}>
          <FormItem
            validateStatus={usernameError ? 'error' : ''}
            help={usernameError || ''}
          >
            {getFieldDecorator('Username', {
              rules: [{ required: true, message: 'Please input your username!' }],
            })(
              <Input prefix={<Icon type="user" style={{ fontSize: 13 }} />} placeholder="Username" />
            )}
          </FormItem>
          <FormItem
            validateStatus={passwordError ? 'error' : ''}
            help={passwordError || ''}
          >
            {getFieldDecorator('Password', {
              rules: [{ required: true, message: 'Please input your Password!' }],
            })(
              <Input prefix={<Icon type="lock" style={{ fontSize: 13 }} />} type="password" placeholder="Password" />
            )}
          </FormItem>
          <FormItem>
            <Button
              type="primary"
              htmlType="submit"
              disabled={hasErrors(getFieldsError())}
            >
              Log in
            </Button>
          </FormItem>
        </Form>
      </div>
    )
  }
}
export default Form.create()(UserClass)


function getTemp()
  pin = 1
  status, temp, humi, temp_dec, humi_dec = dht.read(pin)
  if status == dht.OK then
    print("DHT Temperature:"..temp..";".."Humidity:"..humi)
    return status, temp, humi
  elseif status == dht.ERROR_CHECKSUM then
    print( "DHT Checksum error." )
  elseif status == dht.ERROR_TIMEOUT then
    print( "DHT timed out." )
  end
  return status
end

-- getTemp()

-- if not tmr.create():alarm(2000, tmr.ALARM_AUTO, function()
--   getTemp()
-- end)
-- then
--   print("Unable to create alarm.")
-- end
